var movementsModel = require("../models/movements.model.js");

exports.listMovements = function(req, res){
	var accountId = req.params.accountId;
	movementsModel.listMovements(accountId, res);
};

