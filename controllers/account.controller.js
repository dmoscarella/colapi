var accountModel = require("../models/account.model.js");

exports.listAccounts = function(req, res){
	var userId = req.params.userId;

	accountModel.listAccounts(userId, res);
};

