var loginModel = require("../models/login.model.js");

exports.login = function(req, res){
	var userEmail = req.body.email;
    	var pass = req.body.password;

	loginModel.login(userEmail, pass, res);
};

exports.logout = function(req, res){

	var userEmail = req.body.email;

	loginModel.logout(userEmail, res);
};

