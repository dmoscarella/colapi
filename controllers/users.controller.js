
var usersFile = require("../resources/users.json");

exports.listUsers = function(req, res){

	res.json(usersFile);

};

exports.getUser = function(req, res){

	var userId = req.params.userId;

	var userData = {};

	for(var i in usersFile){
		if(usersFile[i].id == userId){
			userData = usersFile[i];	
		}
	}

	res.json(userData);

};

exports.updateUser =  function(req, res){
	var userId = req.params.userId;

	var userData = req.body;

	for(var i in usersFile){
		if(usersFile[i].id == userId){
			usersFile[i] = userData;	
		}
	}

	res.send("user "+userData.first_name+" updated!");
};

exports.createUser =  function(req, res){
	
	var newUser = req.body;
	
	newUser.id = usersFile.length; 
	usersFile[req.body.id] = newUser;
		
	res.send("user "+newUser.id+": "+newUser.first_name+" registered!");
};

exports.deleteUser = function(req, res){
	res.send("user "+req.params.user+" removed!");
};

