console.log("COLAPI 1.0 starting...");

var express = require("express");
var bodyParser = require("body-parser");
var router = express.Router();

var port = process.env.PORT || 3000;
var app = express();
var apiInfo = {
	"name": "COL API",
	"version": "v1.0",
	"lastUpdated": "2018-05-24",
	"uri": "/colapi/v1"
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use( apiInfo.uri, require('./routes/login.routes.js'));
app.use( apiInfo.uri, require('./routes/users.routes.js'));
app.use( apiInfo.uri, require('./routes/accounts.routes.js'));
app.use( apiInfo.uri, require('./routes/movements.routes.js'));

app.get(apiInfo.uri, function(req, res){
	res.send(apiInfo);
});

app.listen(port, function(){
	console.log("app listening on port "+port+"...");
});



module.exports = app;

