var express = require('express'),
	router = express.Router(),
	accountController = require("../controllers/account.controller.js");


router.get("/users/:userId/accounts", accountController.listAccounts);

module.exports = router;

