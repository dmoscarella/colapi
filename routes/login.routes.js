var express = require('express'),
	router = express.Router(),
	loginController = require("../controllers/login.controller.js");

router.post("/login", loginController.login);
router.post("/logout", loginController.logout);

module.exports = router;

