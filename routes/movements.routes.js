var express = require('express'),
	router = express.Router(),
	movementsController = require("../controllers/movements.controller.js");

router.get("/accounts/:accountId/movements", movementsController.listMovements);

module.exports = router;

