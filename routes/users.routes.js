var express = require('express'),
	router = express.Router(),
	usersController = require("../controllers/users.controller.js");


router.get("/users/", usersController.listUsers);

router.get("/users/:userId", usersController.getUser);

router.put("/users/:user", usersController.updateUser);

router.post("/users", usersController.createUser);

router.delete("/users/:user", usersController.deleteUser);


module.exports = router;

