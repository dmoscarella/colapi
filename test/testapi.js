var app = require("../server.js");
var mocha = require("mocha");
var chai = require("chai");
var chaiHttp = require("chai-http");


var should = chai.should();

chai.use(chaiHttp);


var URL_BASE = "http://localhost:3000";


describe("TEST API", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.get("/colapi/v1")
		.end( (err, res) => {
			res.should.have.status(200);
			done();
		});
	});
});

describe("TEST get user 4", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.get("/colapi/v1/users/4")
		.end( (err, res) => {
			res.body.should.have.property("first_name");
			done();
		});
	});
});


describe("TEST list users", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.get("/colapi/v1/users")
		.end( (err, res) => {
			res.body.should.be.a("array");
			res.body.length.should.be.gt(0);
			done();
		});
	});
});

describe("TEST login dtackell0@twitter.com", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.post("/colapi/v1/login")
		.send({
			"email": "dtackell0@twitter.com",
			"password": "ehiItoJCB4ZR"
		 })
		.end( (err, res) => {
			res.body.should.have.property("first_name");
			done();
		});
	});
});


describe("TEST logout dtackell0@twitter.com", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.post("/colapi/v1/logout")
		.send({ })
		.end( (err, res) => {
			res.body.should.have.property("msg");
			done();
		});
	});
});

describe("TEST list accounts", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.get("/colapi/v1/users/5b0dcb8c9ec05e306bcce242/accounts")
		.end( (err, res) => {
			res.body.should.be.a("array");
			res.body.length.should.be.gt(0);
			done();
		});
	});
});

describe("TEST list movements", () => {
	it("should", (done) => {
		chai.request(URL_BASE)
		.get("/colapi/v1/accounts/5b0dd04b9ec05e306bcce367/movements")
		.end( (err, res) => {
			res.body.should.be.a("array");
			res.body.length.should.be.gt(0);
			done();
		});
	});
});
